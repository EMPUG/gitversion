## Semi Automatic Creation of Version String
The python code retrieves information from the git repo and returns a string with version information such as branch name, number of commits and current commit hash.

Based on https://github.com/nrobinson2000/increment-build/blob/master/build.py

See https://code.visualstudio.com/Docs/editor/tasks

Also https://stackoverflow.com/questions/70804147/run-bash-script-with-vs-code

# Application in python programs
Import the library function
```
from myVersion import CreateVersion
```
Use the imported function to generate the version string
```
# branch and commit only
GitVersion = CreateVersion()
print(GitVersion)

# include file name
prog = sys.argv[0]
prog_parse = prog.split("\\")
prog_name = prog_parse[len(prog_parse) - 1]

GitVersion = prog_name + " " + GitVersion
print(GitVersion)
```
Final output will be
```
main.py master v1.2a0bf40
```

# Application in c programs
Create a tasks.json file in the VSC project .vscode directory or add to the user profile by View -> Command Palette ->  Tasks:Open User Tasks

```
{
    // See https://go.microsoft.com/fwlink/?LinkId=733558
    // for the documentation about the tasks.json format
    "version": "2.0.0",
    "tasks": [
        {
            "label": "myVersion",
            "type": "shell",
            "command": "C:\\Users\\andre\\OneDrive\\code\\Python\\PycharmProjects\\MyVersion\\myVersion.cmd"
        }
    ]
}
```
It's easier if execution of the python script is in a Windows cmd file. Run the VSC task by selecting it in the View -> Command Pallete-> Tasks: Run Task -> myVersion

myVersion.cmd runs the python myVersion.py code that creates the file version.h and contains a single line
```
#define MY_VERSION "version-task v22.0f6741c"
```
You can then retrieve the version from the h file
```
 #include "revision.h"
 String GitVersion = "Unknown";
 
 void setup() {
     GitVersion = MY_VERSION;
     Particle.publish("Branch and version from setup", GitVersion, PRIVATE);
 }
```
## Deployments
phFlameDetector.ino