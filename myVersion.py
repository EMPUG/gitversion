#!/usr/bin/env python3
def CreateVersion():

    # borrowed from https://github.com/nrobinson2000/increment-build/blob/master/build.py
    from subprocess import run, PIPE

    # Create version string from commit number and hash
    count = run(["git", "rev-list", "--count", "HEAD"], stdout=PIPE).stdout.splitlines()[0].decode('utf-8')
    commit = run(["git", "rev-parse", "--short", "HEAD"], stdout=PIPE).stdout.splitlines()[0].decode('utf-8')
    version = "%s.%s" % (count, commit)
    # print(version)

    # get branch name
    # git rev-parse --abbrev-ref HEAD or
    # git branch --show-current
    branch = run(["git", "rev-parse", "--abbrev-ref", "HEAD"], stdout=PIPE).stdout.splitlines()[0].decode('utf-8')
    # print(branch)

    # put them together
    git_info = "%s v%s" % (branch, version)
    return git_info

# test it
if __name__ == "__main__":
    thisGuy = CreateVersion()
    print(thisGuy)
    fh = open("./src/revision.h", "w")
    thisGuy = '#define MY_VERSION ' + '"' + thisGuy + '"'
    fh.write(thisGuy)
    fh.close()
