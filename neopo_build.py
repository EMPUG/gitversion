#!/usr/bin/env python3

# borrowed from https://github.com/nrobinson2000/increment-build/blob/master/build.py
import sys
#import neopo
from subprocess import run, PIPE

# Create version string from commit number and hash
count = run(["git", "rev-list", "--count", "HEAD"], stdout=PIPE).stdout.splitlines()[0].decode('utf-8')
commit = run(["git", "rev-parse", "--short", "HEAD"], stdout=PIPE).stdout.splitlines()[0].decode('utf-8')
VERSION = "%s.%s" % (count, commit)
print(VERSION)

# get branch name
# git rev-parse --abbrev-ref HEAD or
# git branch --show-current
branch = run(["git", "rev-parse", "--abbrev-ref", "HEAD"], stdout=PIPE).stdout.splitlines()[0].decode('utf-8')
print(branch)

# put them together
thisCommit = "%s v%s" % (branch, VERSION)
print(thisCommit)