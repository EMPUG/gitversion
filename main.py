# example of creating version for Python codes
import sys
from myVersion import CreateVersion

# branch and commit only
thisGuy = CreateVersion()
print(thisGuy)

# include file name
prog = sys.argv[0]
prog_parse = prog.split("\\")
prog_name = prog_parse[len(prog_parse) - 1]

thisGuy = prog_name + " " + thisGuy
print(thisGuy)